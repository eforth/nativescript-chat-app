var APPLICATION_ID = '4AE755A7-369F-818B-FFD2-5AE2E1293500';
var SECRET_KEY = '60C5A9D7-15A7-52B8-FF1E-5DBDE4E62B00';
var VERSION = 'v1';
var API_URL = 'https://api.backendless.com';
var HEADERS = {
	"application-id": APPLICATION_ID,
	"secret-key": SECRET_KEY,
	"Content-Type": "application/json",
	"application-type": "REST"
};
var PATHS = {
	SEND_MESSAGE: API_URL + "/<version-name>/messaging/<channel-name>".replace("<version-name>", VERSION),
	SUBSCRIBE_CHATROOM: API_URL + "/<version-name>/messaging/<channel-name>/subscribe".replace("<version-name>", VERSION),
	RETRIEVE_MESSAGES: API_URL + "/<version-name>/messaging/<channel-name>/<subscription-id>".replace("<version-name>", VERSION)
};

module.exports = {
	API_URL:API_URL,
	HEADERS:HEADERS,
	PATHS:PATHS
};