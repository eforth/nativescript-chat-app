var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;

var viewModel = new Observable();

function onLoad(args) {
	var page = args.object;
	var items = new ObservableArray([]);

	var chatroomName = "All Jamaicans"; // hardcoded value - remove later

	var chatroomObject = {
		name: chatroomName,
		id: chatroomName.trim().toLowerCase().replace(/[^a-zA-z0-9-_]/g, "-")
	};

	items.push(chatroomObject);

	viewModel.set("items", items);

	page.bindingContext = viewModel;
}

function onItemTap(args) {
	var index = args.index;
	var chatroomObject = viewModel.items.getItem(index);
	//dialogsModule.alert(chatroomObject.name);

	var NavigationEntry = {
		moduleName: "components/chatroom/chatroom",
		context: chatroomObject
	};

	frameModule.topmost().navigate(NavigationEntry);
}

exports.onLoad = onLoad;
exports.onItemTap = onItemTap;