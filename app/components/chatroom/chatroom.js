var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var API = require("../../api");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var messageList = new ObservableArray([]);
var viewModel = new Observable();

function onLoad(args) {
	var page = args.object;
	var chatroomObject = page.navigationContext;

	viewModel.set("name", chatroomObject.name);
	viewModel.set("id", chatroomObject.id);
	viewModel.set("message", "");
    viewModel.set("messages", messageList);

	page.bindingContext = viewModel;

	subscribeToChannel(chatroomObject.id);
}

function onAdd() {
	createChatroom();
}

function createChatroom() {

	var options = {
		title: "New chatroom",
		message: "Create new chatroom",
		okButtonText: "Create",
		cancelButtonText: "Cancel",
		inputType: dialogsModule.inputType.text
	};

	dialogsModule.prompt(options).then(function(data) {
		console.log(JSON.stringify(data));
	}).catch(function(err) {
		console.log(JSON.stringify(err));
	});
}

function onSend() {
	var message = viewModel.get("message");
	sendMessage(message);
}

// Retrieves the subscription for the current channel (chatroom)
function subscribeToChannel(chatroomId) {

	var CHANNEL_SUBSCRIPTION_URL = API.PATHS.SUBSCRIBE_CHATROOM.replace("<channel-name>", encodeURIComponent(chatroomId));

	fetch(CHANNEL_SUBSCRIPTION_URL, {
		method: "POST",
		headers: API.HEADERS,
		body: "{}"
	}).then(function(response) {
		//console.log(JSON.stringify(response));
		if (!response.ok) throw new Error(response.statusText);
		return response.json();
	}).then(function(data) {
		console.log("subscriptionId", data.subscriptionId);
		viewModel.set("subscriptionId", data.subscriptionId); // Assign the subscriptionId to the view model object for later use
		console.log("Subscribed to channel!");
		pollChatroomMessages();
	}).catch(function(error) {
		console.log(error);
	});
}

// Called after successfully being subscribed to the current channel (chatroom)
function pollChatroomMessages() {
	setInterval(retrieveMessages, 700);
}

// Retrieve messages from the current channel after subscribing to it
// This is called every 700 milliseconds (see line 72)
function retrieveMessages() {
	var subscriptionId = viewModel.get('subscriptionId');
	var chatroomName = viewModel.get('id');
	var RETRIEVE_MESSAGES_URL = API.PATHS.RETRIEVE_MESSAGES.replace("<subscription-id>", subscriptionId).replace("<channel-name>", chatroomName);

	fetch(RETRIEVE_MESSAGES_URL, {
		method: "GET",
		headers: API.HEADERS
	}).then(function(response) {
		if (!response.ok) throw new Error(response.statusText);
		return response.json();
	}).then(function(data) {
		console.log(JSON.stringify(data.messages)); // data.messages is an array of message objects
		console.log("Retrieved messages!");
        addMessagesToList(data.messages); // Called to add messages to the listview
	}).catch(function(error) {
		console.log(error);
	});
}

// Adds messages to the list view via messages observable (see line 16)
function addMessagesToList(messages) {
    messages.forEach(function (message) {
        messageList.push({"text": message.data});
    });
}

// Sends a message to the current channel
function sendMessage(message) {

	var chatroomId = viewModel.get("id");
	var SEND_MESSAGE_URL = API.PATHS.SEND_MESSAGE.replace("<channel-name>", chatroomId);
	// https://api.backendless.com/v1/messages/all-jamaicans
	fetch(SEND_MESSAGE_URL, {
		method: "POST",
		headers: API.HEADERS,
		body: JSON.stringify({ message: message})
	}).then(function(response) {
        if (!response.ok) throw new Error(response.statusText); // throws an error if ok == false
		console.log(JSON.stringify(response));
		return response.json();
	}).then(function(data) {
		dialogsModule.alert("Message sent!");
	}).catch(erroFunc);

	function erroFunc(err) {
		dialogsModule.alert("An error occured while sending the message");
	}

	//fetch --> Promise (Object) --> then() --> function(success)
	//						   --> catch() --> function(fail)
}

function onNavBtnTap() {
	frameModule.topmost().goBack();
}

exports.onLoad = onLoad;
exports.onNavBtnTap = onNavBtnTap;
exports.onSend = onSend;
exports.onAdd = onAdd;

